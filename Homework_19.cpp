﻿#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "voice" << "\n\n";
	} 

	virtual void CleanHeap()
	{
		delete this;
	}
};

class Dog : public Animal
{
	void Voice() override
	{
		std::cout << "Woof!" << "\n\n";
	}

	void CleanHeap() override
	{
		delete this;
	}
};

class Cat : public Animal
{
	void Voice() override
	{
		std::cout << "Meow!" << "\n\n";
	}

	void CleanHeap() override
	{
		delete this;
	}
};

class Fox : public Animal
{
	void Voice() override
	{
		std::cout << "What does the fox say?!" << "\n\n";
	}

	void CleanHeap() override
	{
		delete this;
	}
};

int main()
{ 
	Animal* animals[] = { new Dog, new Cat, new Fox };

	for (int i = 0; i < 3; i++)
	{
		animals[i]->Voice();
		animals[i]->CleanHeap();
	}

	return 0;
}
